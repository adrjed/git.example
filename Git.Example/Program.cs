﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Git.Example
{
	class Program
	{
		static void Main( string[] args )
		{
			Console.WriteLine( "Wypisanie liczb od 0 do 10" );

			int[] intArray = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
			foreach (var i in intArray)
			{
				Console.WriteLine( "$Liczba {i}" );
			}

			Console.ReadLine();
			// Przykład pętli foreach wypisującej liczby
		}
	}
}
